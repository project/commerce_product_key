<?php

/**
 * @file
 * Builds placeholder replacement tokens for commerce product keys.
 */


/**
 * Implements hook_token_info().
 */
function commerce_product_key_token_info() {

  $info['type']['commerce-product-keys'] = array(
    'name' => t('Product keys'),
    'description' => t('Tokens related to commerce product keys.'),
    'needs-data' => 'commerce-product-keys',
  );

  $info['tokens']['commerce-order']['product-keys'] = array(
    'name' => t('Product keys'),
    'description' => t('The product keys assigned to this order.'),
    'type' => 'commerce-product-keys',
  );

  $info['tokens']['commerce-product-keys']['html-list'] = array(
    'name' => t('HTML <ul> list'),
    'description' => t('An HTML <ul> list of product keys.'),
  );

  $info['tokens']['commerce-product-keys']['text'] = array(
    'name' => t('Text list'),
    'description' => t('A comma-separated text list of product keys.'),
  );

  $info['tokens']['commerce-product-keys']['text-bullets'] = array(
    'name' => t('Text list'),
    'description' => t('A bulleted text list of product keys.'),
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function commerce_product_key_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'commerce-order' && !empty($data['commerce-order'])) {
    $order = $data['commerce-order'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Simple key values on the order.
        case 'product-keys':
          // If the user asks for the product keys without specifying a format,
          // use the comma-separated text format.
          $product_keys = commerce_product_key_load_multiple(array(), array('order_id' => $order->order_id));
          $codes = array();
          foreach ($product_keys as $product_key) {
            $codes[] = $sanitize ? filter_xss($product_key->code) : $product_key->code;
          }
          $replacements[$original] = implode(', ', $codes);
          break;
      }
    }

    // Chained token relationships.
    if ($format_tokens = token_find_with_prefix($tokens, 'product-keys')) {
      if ($product_keys = commerce_product_key_load_multiple(array(), array('order_id' => $order->order_id))) {
        $replacements += token_generate('commerce-product-keys', $format_tokens, array('commerce-product-keys' => $product_keys), $options);
      }
    }
  }

  if ($type == 'commerce-product-keys' && !empty($data['commerce-product-keys'])) {
    $product_keys = $data['commerce-product-keys'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Simple key values (formats) for the list of product keys.
        case 'html-list':
          $output = '<ul>';
          foreach ($product_keys as $product_key) {
            $output .= '<li>' . ($sanitize ? filter_xss($product_key->code) : $product_key->code) . '</li>';
          }
          $output .= '</ul>';
          $replacements[$original] = $output;
          break;
        case 'text':
          $codes = array();
          foreach ($product_keys as $product_key) {
            $codes[] = $sanitize ? filter_xss($product_key->code) : $product_key->code;
          }
          $replacements[$original] = implode(', ', $codes);
          break;
        case 'text-bullets':
          $output = '';
          foreach ($product_keys as $product_key) {
            $output .= '• ' . ($sanitize ? filter_xss($product_key->code) : $product_key->code) . "\n";
          }
          $replacements[$original] = $output;
          break;
      }
    }
  }

  return $replacements;
}
